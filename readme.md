## Cycle Appliance Deployment Repository (cycle-ci-app-deploy)
This repository contains the ARM templates used to deploy the Cycle Appliance into an Azure tenant. This ARM template will create and configure the resources into Azure after the inputs are entered and the deployment is configured to the customer's specifications.

[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fmy-rc.cycleautomation.com%2Flabtest%2Fazuredeploy.json)

##  Technology Used
The ARM template is a file written in JSON declarative syntax. It is a file used to deploy and create infrastructure in Microsoft's public Azure Cloud. All of the resources deployed within the template make calls to Azure to create those resources using a specified version of that resource's API. We also use a Cloud Initialization script as part of this ARM template to run scripts inside of the Ubuntu virtual machine provisioned as a Jenkins manager with the ARM template. We have a Deep Dive document that goes over the deployment and usage of the Cycle Appliance that is linked [here](https://my.cycleautomation.com/user-manual/cycle_appliance/)

## Folders
The folder structure in the repo is meant to keep it organized and also put our nested templates/artifacts in subfolders that make sense. The folder structure has been kept simple and functional, and is explained below in detail.

**architecture:** Architectural diagrams to explain the deployment.

**arm-template:** Where the ARM templates are stored. There is a parent template and as of 06/05/2020, there is one nested template that gets invoked from the parent to build the Azure SQL datastore.

**other:** We stored some example scripts in here to be used for post-deployment configuration. Things like initialization scripts for the Jenkins Azure VM Agent plugin, etc. 