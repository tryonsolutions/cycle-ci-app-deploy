-- 2.2.0

CREATE TABLE cycle_test_execution (
execution_id nvarchar(100) NOT NULL,
execution_start_ts datetime2(7) NOT NULL,
cycle_user nvarchar(100) NOT NULL,
cycle_version nvarchar(100) NOT NULL,
hardware_id nvarchar(100) NOT NULL,
computer_username nvarchar(100) NOT NULL,
os nvarchar(100) NOT NULL,
invoker nvarchar(100) NOT NULL,
CONSTRAINT PK_test_execution PRIMARY KEY (execution_id)
) ;

CREATE TABLE cycle_execution_results (
execution_id nvarchar(100) NOT NULL,
node_id nvarchar(255) NOT NULL,
parent_node_id nvarchar(255) DEFAULT NULL,
sending_node_id nvarchar(255) NOT NULL,
node_sequence int NOT NULL,
node_type nvarchar(100) NOT NULL,
block_type nvarchar(100) DEFAULT NULL,
name TEXT DEFAULT NULL,
status nvarchar(100) NOT NULL,
message TEXT DEFAULT NULL,
error_message TEXT DEFAULT NULL,
file_uri TEXT DEFAULT NULL,
start_line int DEFAULT NULL,
end_line int DEFAULT NULL,
data_file_path TEXT DEFAULT NULL,
worker_name TEXT DEFAULT NULL,
tags TEXT DEFAULT NULL,
step_id nvarchar(100) DEFAULT NULL,
delay_start_time datetime2(7) DEFAULT NULL,
start_time datetime2(7) NOT NULL,
end_time datetime2(7) DEFAULT NULL,
CONSTRAINT PK_cycle_execution_results PRIMARY KEY (execution_id,node_id),
CONSTRAINT FK_cycle_execution_results FOREIGN KEY (execution_id) REFERENCES cycle_test_execution(execution_id)
) ;
CREATE  INDEX cycle_node_type_IDX ON cycle_execution_results (node_type);;
CREATE  INDEX cycle_parent_node_IDX ON cycle_execution_results (execution_id,parent_node_id);;
CREATE  INDEX cycle_sending_node_IDX ON cycle_execution_results (execution_id,sending_node_id);;

-- 2.3.0

CREATE TABLE cycle_image (
  id nvarchar(100) NOT NULL,
  image varbinary(MAX),
  CONSTRAINT PK_CYCLE_IMAGE PRIMARY KEY (id)
 )  ;
CREATE TABLE cycle_image_results (
  id nvarchar(100) NOT NULL,
  execution_id nvarchar(100) NOT NULL,
  node_id nvarchar(255) NOT NULL,
  template_file TEXT,
  template_image_id nvarchar(100) DEFAULT NULL,
  screenshot_image_id nvarchar(100) DEFAULT NULL,
  found_image BIT DEFAULT NULL,
  min_value FLOAT DEFAULT NULL,
  max_value FLOAT DEFAULT NULL,
  min_location_x int DEFAULT NULL,
  min_location_y int DEFAULT NULL,
  max_location_x int DEFAULT NULL,
  max_location_y int DEFAULT NULL,
  source_width int DEFAULT NULL,
  source_height int DEFAULT NULL,
  template_width int DEFAULT NULL,
  template_height int DEFAULT NULL,
  correlation_threshold FLOAT DEFAULT NULL,
  CONSTRAINT PK_CYCLE_IMAGE_RESULTS PRIMARY KEY (id) ,
  CONSTRAINT FK_CYCLE_IMAGE_RESULTS FOREIGN KEY (execution_id, node_id) REFERENCES cycle_execution_results(execution_id,node_id) ,
  CONSTRAINT FK_CYCLE_TERMINAL_T FOREIGN KEY (template_image_id) REFERENCES cycle_image(id) ,
  CONSTRAINT FK_CYCLE_TERMINAL_S FOREIGN KEY (screenshot_image_id) REFERENCES cycle_image(id)
 )  ;


CREATE TABLE cycle_terminal_screens (
  id nvarchar(100) NOT NULL,
  execution_id nvarchar(100) NOT NULL,
  node_id nvarchar(255) NOT NULL,
  screen TEXT,
  CONSTRAINT PK_CYCLE_TERMINAL PRIMARY KEY (id) ,
  CONSTRAINT FK_CYCLE_TERMINAL FOREIGN KEY (execution_id,node_id) REFERENCES cycle_execution_results(execution_id,node_id)
 )  ;


CREATE TABLE cycle_file_diagnostics (
  id nvarchar(100) NOT NULL,
  execution_id nvarchar(100) NOT NULL,
  node_id nvarchar(255) NOT NULL,
  start_line int,
  start_character int,
  end_line int,
  end_character int,
  message TEXT,
  severity nvarchar(100),
  code int,
  source TEXT,
  CONSTRAINT PK_FILE_DIAGNOSTICS PRIMARY KEY (id) ,
  CONSTRAINT FK_FILE_DIAGNOSTICS FOREIGN KEY (execution_id,node_id) REFERENCES cycle_execution_results(execution_id,node_id)
 )  ;

 -- 2.5.0

 CREATE TABLE data_type_lookup (
  id int NOT NULL ,
  datatype_desc nvarchar(100) NOT NULL ,
  CONSTRAINT data_type_lookup_PK PRIMARY KEY (id)
 )  ;


insert into data_type_lookup values (1, 'STRING');
insert into data_type_lookup values (2, 'INT');
insert into data_type_lookup values (3, 'FLOAT');

CREATE TABLE test_plan (
  id nvarchar(100) NOT NULL ,
  name nvarchar(255) NOT NULL ,
  is_default BIT  ,
  description TEXT  ,
  CONSTRAINT test_plan_PK PRIMARY KEY (id)
 )  ;



CREATE TABLE test_data_tag (
  id nvarchar(100) NOT NULL ,
  name nvarchar(255) NOT NULL ,
  CONSTRAINT test_data_tag_PK PRIMARY KEY (id)
 )  ;



CREATE TABLE test_data_set (
  id nvarchar(100) NOT NULL ,
  test_plan_id nvarchar(100) NOT NULL ,
  name nvarchar(255) NOT NULL ,
  description TEXT  ,
  CONSTRAINT test_data_set_PK PRIMARY KEY (id) ,
  CONSTRAINT test_data_set_FK FOREIGN KEY (test_plan_id) REFERENCES test_plan(id)
 )  ;
 CREATE UNIQUE INDEX test_data_set_UN ON test_data_set (test_plan_id,name);


CREATE TABLE test_data_column (
  id nvarchar(100) NOT NULL ,
  test_data_set_id nvarchar(100) NOT NULL ,
  name nvarchar(255) NOT NULL ,
  datatype int NOT NULL ,
  sort_sequence int NOT NULL ,
  CONSTRAINT test_data_column_PK PRIMARY KEY (id) ,
  CONSTRAINT test_data_column_FK FOREIGN KEY (test_data_set_id) REFERENCES test_data_set(id) ,
  CONSTRAINT test_data_type_FK FOREIGN KEY (datatype) REFERENCES data_type_lookup(id)
 )  ;



CREATE TABLE test_data_record (
  id nvarchar(100) NOT NULL ,
  test_data_set_id nvarchar(100) NOT NULL ,
  sort_sequence int NOT NULL ,
  CONSTRAINT test_data_record_PK PRIMARY KEY (id) ,
  CONSTRAINT test_data_record_FK FOREIGN KEY (test_data_set_id) REFERENCES test_data_set(id)
 )  ;



CREATE TABLE test_data_record_tag (
  id nvarchar(100) NOT NULL ,
  test_data_tag_id nvarchar(100) NOT NULL ,
  test_data_record_id nvarchar(100) NOT NULL ,
  CONSTRAINT test_data_record_tag_PK PRIMARY KEY (id) ,
  CONSTRAINT test_data_tag_FK FOREIGN KEY (test_data_tag_id) REFERENCES test_data_tag(id) ,
  CONSTRAINT test_data_record_tag_FK FOREIGN KEY (test_data_record_id) REFERENCES test_data_record(id)
 )  ;



CREATE TABLE test_data_value (
  id nvarchar(100) NOT NULL ,
  test_data_record_id nvarchar(100) NOT NULL ,
  test_data_column_id nvarchar(100) NOT NULL ,
  data_value TEXT  ,
  CONSTRAINT test_data_value_PK PRIMARY KEY (id) ,
  CONSTRAINT test_data_record_val_FK FOREIGN KEY (test_data_record_id) REFERENCES test_data_record(id) ,
  CONSTRAINT test_data_column_val_FK FOREIGN KEY (test_data_column_id) REFERENCES test_data_column(id)
 )  ;

 -- 2.7.0

ALTER TABLE test_data_tag
 ADD CONSTRAINT test_data_tag_UC UNIQUE (name);
ALTER TABLE test_data_column
 ADD CONSTRAINT test_data_column_UC UNIQUE (test_data_set_id,name);

 -- 2.7.1
CREATE INDEX cycle_test_start_ts_IDX ON cycle_test_execution (execution_start_ts);
CREATE INDEX cycle_node_sequence_IDX ON cycle_execution_results (node_sequence);

-- populate versioning

CREATE TABLE cycle_version (
  id nvarchar(100) NOT NULL,
  major int NOT NULL,
  minor int NOT NULL,
  revision int NOT NULL,
  state nvarchar(100) DEFAULT NULL,
  CONSTRAINT PK_CYCLE_VERSION PRIMARY KEY (id)
 );

CREATE TABLE cycle_version_detail (
  id nvarchar(100) NOT NULL,
  version_id nvarchar(100) NOT NULL,
  hash nvarchar(255) NOT NULL,
  applied_at datetime2(7) NOT NULL,
  apply_script TEXT DEFAULT NULL,
  state nvarchar(100) DEFAULT NULL,
  last_problem TEXT DEFAULT NULL,
  CONSTRAINT PK_CYCLE_VERSION_DETAIL PRIMARY KEY (id) ,
  CONSTRAINT FK_CYCLE_VERSION FOREIGN KEY (version_id) REFERENCES cycle_version(id)
 );

 insert into cycle_version (id, major, minor, revision, state)
  values
   ('2.2.0',2,2,0,'applied'),
   ('2.3.0',2,3,0,'applied'),
   ('2.5.0',2,5,0,'applied'),
   ('2.7.0',2,7,0,'applied'),
   ('2.7.1',2,7,1,'applied');
