const fs = require("fs");
const path = require('path');
const { argv } = require("process");

if (argv.length < 3) {
  usageError()
}

let paths = getFilePathsFromArgs(process.argv);
console.log(`Compiling ${paths.armTemp}, by including ${paths.cloudInit}`)

let armTempJson = JSON.parse(fs.readFileSync(paths.armTemp, 'utf8'));
armTempJson = compileCustomData(armTempJson, paths.cloudInit);
writeArmTemp(armTempJson, paths.armTemp);


/**
 * Prints out helper info and exits
 */
function usageError() {
  console.log("Requires 3 arguments.")
  console.log("Arg 1: Path to pre-compiled ARM template JSON file.");
  console.log("Arg 2: Path to cloud-init file.");

  process.exit(1);
}

/**
 * Parses the file paths out from the cli args.
 * @param {Array[String]} args Arguments from CLI input
 */
function getFilePathsFromArgs(args) {
  let armTemp = path.normalize(args.slice(2)[0]);
  let cloudInit = path.normalize(args.slice(3)[0])

  return {
    armTemp,
    cloudInit
  }
}

/**
 * Creates the String for the 
 * @param {JSON} armTempJson The JSON of the base ARM template.
 * @param {String} cloudInitPath Path to the cloud-init file
 */
function compileCustomData(armTempJson, cloudInitPath) {
  let content = fs.readFileSync(cloudInitPath).toString()
  content = `[base64(concat('${content}'))]`
  armTempJson.variables.customData = content;

  return armTempJson
}

/**
 * Writes the ARM Template JSON to the passed in ARM Template file path.
 * @param {JSON} armTempJson The JSON of the base ARM template.
 * @param {String} armTempPath Path to write the compiled ARM template.
 */
function writeArmTemp(armTempJson, armTempPath) {
  fs.writeFileSync(armTempPath, JSON.stringify(armTempJson, null, 4));
}