param
(
      [string]$adminUserName,
      [string]$adminPublicKey,
      [string]$voiceUserName,
      [string]$voicePublickey
)

#Allow firewall
New-NetFirewallRule -Name sshd -DisplayName 'Cygwin SSH' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

# Create vocollect-users group, local user, and then add that user to the group.
New-LocalGroup -Name "vocollect-users"
New-LocalUser -Name "$voiceUserName" -Description "The user who will use the Vocollect emulator" -NoPassword
Add-LocalGroupMember -Group "vocollect-users" -Member "$voiceUserName"
Add-LocalGroupMember -Group "Users" -Member "$voiceUserName"


#Install cygwin x64
Write-Host "Installing Cygwin x64..." -ForegroundColor Cyan

if (Test-Path C:\cygwin64) {
    Write-Host "Deleting existing installation..."
    Remove-Item C:\cygwin64 -Recurse -Force
}

# download installer
New-Item -Path C:\cygwin64 -ItemType Directory -Force
$exePath = "C:\cygwin64\setup-x86_64.exe"
(New-Object Net.WebClient).DownloadFile('https://cygwin.com/setup-x86_64.exe', $exePath)

# install cygwin
cmd /c start /wait $exePath -qnNdO -R C:/cygwin64 -s http://cygwin.mirror.constant.com -l C:/cygwin64/var/cache/setup -P mingw64-i686-gcc-g++ -P mingw64-x86_64-gcc-g++ -P gcc-g++ -P autoconf -P automake -P bison -P libtool -P make -P python -P gettext-devel -P intltool -P libiconv -P pkg-config -P wget -P curl -P openssh -P dos2unix
C:\cygwin64\bin\bash -lc true

Write-Host "Installed Cygwin x64" -ForegroundColor Green

# Set path so that we can reference *nix programs and then run bash commands on Cygwin!
$env:path = "$($env:path);c:\cygwin64\bin"
cd c:\cygwin64\bin ; .\bash.exe -c "/bin/mkpasswd -l > /etc/passwd&&/bin/mkgroup -l > /etc/group&&/bin/ssh-host-config -y"

#
New-Item -Path C:\cygwin64\home\$voiceUserName\.ssh -ItemType Directory -Force 
echo "$voicePublicKey" | Add-Content C:\cygwin64\home\$voiceUserName\.ssh\authorized_keys
New-Item -Path C:\cygwin64\home\$adminUserName\.ssh -ItemType Directory -Force 
echo "$adminPublicKey" | Add-Content C:\cygwin64\home\$adminUserName\.ssh\authorized_keys

# Download our script
$WebClient = New-Object System.Net.WebClient
$WebClient.DownloadFile("https://bitbucket.org/tryonsolutions/cycle-ci-app-deploy/raw/PROD-HEAD/other/scripts/set-perms.sh","C:\cygwin64\set-perms.sh")

# Run our script
C:\cygwin64\bin\bash.exe -c "/cygdrive/c/cygwin64/set-perms.sh $voiceUserName"
C:\cygwin64\bin\bash.exe -c "/cygdrive/c/cygwin64/set-perms.sh $adminUserName"

# Edit permissions on sshd_config, and create vocollect folder and edit permissions on it
cd c:\cygwin64\bin ; .\bash.exe -c "/bin/chmod g+w /etc/sshd_config&&/bin/mkdir -p /vocollect/emulator&&/bin/chmod +x -R /vocollect/*"


# Edit SSHd config
Add-Content C:\cygwin64\etc\sshd_config "
PasswordAuthentication no
ClientAliveInterval 1m
ClientAliveCountMax 1

# Example of overriding settings on a per-user basis
Match group vocollect-users
    X11Forwarding no
    AllowTcpForwarding yes
    ForceCommand /vocollect/menu.sh"

#Start cygsshd service so that users can connect
Start-Service cygsshd