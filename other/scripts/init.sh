# Setting variables from customScript extension
USERNAME=$1
PASSWORD=$2
SERVERNAME=$3
DATABASE=cycledatastore

# Prepping SQL Database using sql-tools Docker container
# Allowing more time for SQL tools to be downloaded and installed. We've seen variance on how long it takes to get all of the files in place, so we went from 5 minutes to 6 minutes on the sleep. 
sleep 360
cd /opt/mssql-tools/bin/
./sqlcmd -S $SERVERNAME.database.windows.net -d $DATABASE -U $USERNAME -P $PASSWORD -i /var/lib/waagent/custom-script/download/0/mssql_datastore_init.sql
